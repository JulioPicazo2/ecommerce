import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';

import { HomeComponent } from './pages/home/home.component';

import { ProductsComponent } from './pages/products/products.component';
import { FormProductsComponent } from './pages/products/form-products/form-products.component';

import { AdminsComponent } from './pages/admins/admins.component';
import { FormAdminsComponent } from './pages/admins/form-admins/form-admins.component';

import { TicketComponent } from './pages/ticket/ticket.component';
import { SellsComponent } from './pages/sells/sells.component';

import { NotFoundComponent } from './pages/not-found/not-found.component';
import { CommingSoonComponent } from './pages/comming-soon/comming-soon.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },

  { path: 'home', component: HomeComponent },

  { path: 'products', component: ProductsComponent },
  { path: 'add-products/:id', component: FormProductsComponent },

  { path: 'admins', component: AdminsComponent },
  { path: 'add-admins/:id', component: FormAdminsComponent },

  { path: 'ticket', component: TicketComponent },

  { path: 'sells', component: SellsComponent },

  { path: 'not-found', component: NotFoundComponent },
  
  { path: 'comming-soon', component: CommingSoonComponent },

  { path: '**', pathMatch: 'full', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule {}