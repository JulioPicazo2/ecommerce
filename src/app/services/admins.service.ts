import { Injectable } from '@angular/core';
import { AdminsModel } from '../models/admins.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminsService {
    
    search: string;
    urlData: string = 'http://localhost:80/backend';
    products: AdminsModel[] = [];
  
    constructor( private httpClient: HttpClient ) { 
    }
    
    dataTableAdmins() {
      return this.httpClient.get(`${this.urlData}/seleccionarUsuarioTodos.php`).toPromise()
      .catch(err => {
        return err.error;
      });
    }
  
    addAdmin(admin: AdminsModel) {
      return this.httpClient.put(`${this.urlData}/agregarUsuario.php`,JSON.stringify(admin)).toPromise();
    }
  
    getID() {
      return this.httpClient.get(`${this.urlData}/getIDAdmin.php`);
    }

    getAdmin(nmbID) {
      return this.httpClient.get<AdminsModel>(`${this.urlData}/seleccionarUsuario.php?nmbID=${nmbID}`).toPromise();
    }
  
    modifyAdmin(admin: AdminsModel) {
      return this.httpClient.put(`${this.urlData}/modificacionUsuario.php/${admin.nmbID}`, admin).toPromise();
    }
  
    deleteAdmin(nmbID: string) {
      return this.httpClient.delete(`${this.urlData}/eliminarUsuario.php?nmbID=${nmbID}`).toPromise();
    }  
  
}