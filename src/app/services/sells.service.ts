import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SellsModel } from './../models/sells.model';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Injectable({
  providedIn: 'root'
})
export class SellsServiceService {

  urlData: string = 'http://localhost:80/backend';
  search:string;
  products: SellsModel[] = [];

  
  constructor( private httpClient: HttpClient ) { 
  }

  dataTableProducts() {
    return this.httpClient.get(`${this.urlData}/seleccionarVentasTodos.php`).toPromise()
    .catch(err => {
      return err.error;
    });
  }

  addProduct(product: SellsModel) {
    return this.httpClient.put(`${this.urlData}/agregarProducto.php`,JSON.stringify(product)).toPromise();
  }

  getProduct(nmbCode: string) {
    return this.httpClient.get(`${this.urlData}/seleccionarProducto.php?nmbCode=${nmbCode}`).toPromise();
  }

  modifyProduct(product: SellsModel) {
    return this.httpClient.put(`${this.urlData}/modificacionProducto.php/${product.nmbCode}`, product).toPromise();
  }

  deleteProduct(nmbCode: string) {
    return this.httpClient.delete(`${this.urlData}/eliminarProducto.php?nmbCode=${nmbCode}`).toPromise();
  }
}
