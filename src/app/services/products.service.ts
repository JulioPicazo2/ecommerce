import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductsModel } from '../models/products.model';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  urlData: string = 'http://localhost:80/backend';
  search:string;
  products: ProductsModel[] = [];

  constructor( private httpClient: HttpClient ) { 
  }

  dataTableProducts() {
    return this.httpClient.get(`${this.urlData}/seleccionarProductosTodos.php`).toPromise()
    .catch(err => {
      return err.error;
    });
  }

  addProduct(product: ProductsModel) {
    return this.httpClient.put(`${this.urlData}/agregarProducto.php`,JSON.stringify(product)).toPromise();
  }

  getProduct(nmbCode) {
    return this.httpClient.get<ProductsModel>(`${this.urlData}/seleccionarProducto.php?nmbCode=${nmbCode}`).toPromise();
  }

  modifyProduct(product: ProductsModel) {
    return this.httpClient.put(`${this.urlData}/modificacionProducto.php/${product.nmbCode}`, product).toPromise();
  }

  deleteProduct(nmbCode: string) {
    return this.httpClient.delete(`${this.urlData}/eliminarProducto.php?nmbCode=${nmbCode}`).toPromise();
  }

  getID() {
    return this.httpClient.get(`${this.urlData}/getIDProduct.php`);
  }

}