import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatatableSellsComponent } from './datatable-sells.component';

describe('DatatableSellsComponent', () => {
  let component: DatatableSellsComponent;
  let fixture: ComponentFixture<DatatableSellsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatatableSellsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatatableSellsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
