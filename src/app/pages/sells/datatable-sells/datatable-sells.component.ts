import { Component, OnInit } from '@angular/core';
import { SellsServiceService } from './../../../services/sells.service';
import { SellsModel } from 'src/app/models/sells.model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});
  

@Component({
  selector: 'app-datatable-sells',
  templateUrl: './datatable-sells.component.html',
  styleUrls: ['./datatable-sells.component.css']
})
export class DatatableSellsComponent implements OnInit {
  actualPage: number = 1;
  sells: SellsModel[] = [];

  constructor( private productsService: SellsServiceService, private router: Router ) { }

  ngOnInit() {
    this.getDataTableProducts();
  }
  
  getDataTableProducts() {
   this.productsService.dataTableProducts().then((datos: SellsModel[]) => {
      this.sells = datos;

      console.log(this.sells);
    }).catch(() => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Connection lost'
      });
    });
  }
  
  getProduct(id_venta: string) {
    this.productsService.getProduct(id_venta).then((result: any) => this.sells = result[0]);
  }

  deleteProduct( product: SellsModel ) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.productsService.deleteProduct(product.nmbCode).then(data => {
          Toast.fire({
            type: 'success',
            title: `The product ${product.nmbCode} has been deleted!`,
          });
          return this.getDataTableProducts();
        }).catch(() => {
          return Toast.fire({
            type: 'error',
            title: 'Oops... sorry we have some problems! :(',
          });
        });
      }
    });
  }


}
