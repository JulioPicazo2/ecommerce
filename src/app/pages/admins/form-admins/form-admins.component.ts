import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatesService } from './../../../services/states.service';
import { AdminsModel } from '../../../models/admins.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminsService } from 'src/app/services/admins.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Component({
  selector: 'app-form-admins',
  templateUrl: './form-admins.component.html',
  styleUrls: ['./form-admins.component.css']
})
export class FormAdminsComponent implements OnInit {
  states: any = [];
  admins: AdminsModel = new AdminsModel();
  form: FormGroup;
  num: number;

  constructor( private router: Router, private adminsService: AdminsService ,private statesService: StatesService, private activatedRoute: ActivatedRoute ) {
    this.adminsService.getID().subscribe((resp: any) => {
      this.num = resp.mensaje;
      this.form.controls['nmbID'].setValue(this.num);
    });
    this.states = this.statesService.getStates();
    this.form = new FormGroup({
      'nmbID': new FormControl(this.num, [Validators.required]),
      'strLastName': new FormControl(this.admins.strLastName, [Validators.required]),
      'strSecondLastName': new FormControl(this.admins.strSecondLastName, [Validators.required]),
      'strName': new FormControl(this.admins.strName, [Validators.required]),
      'strUsername': new FormControl(this.admins.strUsername, [Validators.required]),
      'strCellphone': new FormControl(this.admins.strCellphone, [Validators.required]),
      'strEmail': new FormControl(this.admins.strEmail, [Validators.required, Validators.pattern('[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}')]),
      'strPassword': new FormControl(this.admins.strPassword, [Validators.required, Validators.min(8)]),
      'strPasswordConfirm': new FormControl(),
      'strAddress': new FormControl(this.admins.strAddress, [Validators.required]),
      'strAddress2': new FormControl(this.admins.strAddress2, [Validators.required]),
      'strCountry': new FormControl('Mexico', [Validators.required]),
      'strState': new FormControl('', [Validators.required]),
      'strCity': new FormControl(this.admins.strCity, [Validators.required]),
      'strZip': new FormControl(this.admins.strZip, [Validators.required, Validators.min(5)])
    });
    this.form.controls['strPasswordConfirm'].setValidators([Validators.required, this.validatingPassword.bind(this.form)]);
  }

  ngOnInit() {
    const nmbCode = this.activatedRoute.snapshot.params.id;
    if (nmbCode !== 'new') {
      this.adminsService.getAdmin(nmbCode).then((admin: AdminsModel) => {
        this.admins = admin['admin'];
        this.form = new FormGroup({
          'nmbID': new FormControl(this.admins.nmbID, [Validators.required]),
          'strLastName': new FormControl(this.admins.strLastName, [Validators.required]),
          'strSecondLastName': new FormControl(this.admins.strSecondLastName, [Validators.required]),
          'strName': new FormControl(this.admins.strName, [Validators.required]),
          'strUsername': new FormControl(this.admins.strUsername, [Validators.required]),
          'strCellphone': new FormControl(this.admins.strCellphone, [Validators.required]),
          'strEmail': new FormControl(this.admins.strEmail, [Validators.required, Validators.pattern('[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}')]),
          'strPassword': new FormControl(this.admins.strPassword, [Validators.required, Validators.min(8)]),
          'strPasswordConfirm': new FormControl(),
          'strAddress': new FormControl(this.admins.strAddress, [Validators.required]),
          'strAddress2': new FormControl(this.admins.strAddress2, [Validators.required]),
          'strCountry': new FormControl('Mexico', [Validators.required]),
          'strState': new FormControl(this.admins.strState ,[Validators.required]),
          'strCity': new FormControl(this.admins.strCity, [Validators.required]),
          'strZip': new FormControl(this.admins.strZip, [Validators.required, Validators.min(5)])
        });
        this.form.controls['strPasswordConfirm'].setValidators([Validators.required, this.validatingPassword.bind(this.form)]);
      }).catch(() => {
        return Toast.fire({
          type: 'error',
          title: 'Oops... sorry we have some problems! :(',
        });
      });
    }
    this.getStates();
  }

  getStates() {
    this.states = this.statesService.getStates();
  }

  modifyAdmin() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You can modify in the future!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, add it!'
    }).then((result) => {
      this.admins = this.form.value;
      console.log(this.form);
      this.adminsService.modifyAdmin(this.admins).then((data) => {
        Toast.fire({
          type: 'success',
          title: `The administrator ${this.admins.strName} has been modified`
        });
      });
    }).catch(() => {
      Toast.fire({
        type: 'error',
        title: 'No jala'
      });
    });
  }

  addAdmin() {
    if (this.form.invalid) {
      return Swal.fire({
        title:'Oops...',
        text: 'Fill all the fields to continue',
        type:'error'
      });
    } else if(this.admins.nmbID != null) {
      this.modifyAdmin();
    } else {
      Swal.fire({
        title: 'Are you sure?',
        text: 'You can modify in the future!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, add it!'
      }).then((result) => {
        if (result.value) {
          this.admins = this.form.value;
          this.adminsService.addAdmin(this.admins).then((data) => {
            Swal.fire({ 
              type: 'success',
              title: `Success`,
              text: `The product ${this.admins.strName} has been added. ` +
              'Dou  yow want to add another?',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, want it!',
              cancelButtonText: 'No, thanks'      
            }).then((result) => {
              if(result.value) {
                this.form.reset();
              } else {
                this.router.navigate(['/admins']);
              }
            });
          }).catch(() => {
            return Toast.fire({
              type: 'error',
              title: 'Oops... sorry we have some problems! :(',
            });
          });
        }
      });
    }
  }

  validatingPassword( formControl: FormControl ): { [s:string]: boolean } {
    let form: any = this;
    if ( formControl.value !== form.controls['strPassword'].value ) {
      return {
        validating: true
      }
    }
    return null;
  }

}