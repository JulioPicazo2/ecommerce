import { Component, OnInit, Input } from '@angular/core';
import { AdminsModel } from './../../models/admins.model';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.css']
})
export class AdminsComponent implements OnInit {

  @Input() admin: AdminsModel;

  constructor() { }

  ngOnInit() {
  }

}
