import { Component, OnInit, Input } from '@angular/core';
import { AdminsModel, AdminsInterface } from '../../../models/admins.model';
import { StatesService } from './../../../services/states.service';
import { AdminsService } from './../../../services/admins.service';
import { ActivatedRoute } from '@angular/router'
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Component({
  selector: 'app-modify-admins',
  templateUrl: './modify-admins.component.html',
  styleUrls: ['./modify-admins.component.css']
})
export class ModifyAdminsComponent implements OnInit {
  
  @Input() admins: AdminsModel;
  
  states: any = [];

  admin: AdminsInterface = {
    nmbID: null,
    strLastName: null,
    strSecondLastName: null,
    strName: null,
    strUsername: null,
    strCellphone: null,
    strEmail: null,
    strPassword: null,
    strPasswordConfirm: null,
    strAddress: null,
    strAddress2: null,
    strCountry: 'Mexico',
    strState: null,
    strCity: null,
    strZip: null
  };

  constructor( private activatedRoute: ActivatedRoute, private adminsService: AdminsService, private statesService: StatesService ) { 
  
  }

  ngOnInit() {
  }

}
