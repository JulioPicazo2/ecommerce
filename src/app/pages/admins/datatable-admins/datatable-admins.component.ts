import { Component, OnInit } from '@angular/core';
import { AdminsService } from './../../../services/admins.service';
import { Router } from '@angular/router';
import { AdminsModel } from 'src/app/models/admins.model';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Component({
  selector: 'app-datatable-admins',
  templateUrl: './datatable-admins.component.html',
  styleUrls: ['./datatable-admins.component.css']
})
export class DatatableAdminsComponent implements OnInit {

  actualPage: number = 1;
  admins: AdminsModel[] = [];

  constructor(private adminsService: AdminsService, private router: Router) { }
  
  ngOnInit() {
    this.getDataTableAdmins();
  }

  getDataTableAdmins() {
    this.adminsService.dataTableAdmins().then((datos: AdminsModel[]) => {
       this.admins = datos;
     }).catch(() => {
       return Swal.fire({
         type: 'error',
         title: 'Oops...',
         text: 'Connection lost'
       });
     });
   }

   getProduct(nmbCode: string) {
    this.adminsService.getAdmin(nmbCode).then((result: any) => this.admins = result[0]);
  }

   deleteAdmin( admin: AdminsModel ) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.adminsService.deleteAdmin(admin.nmbID).then(data => {
          Toast.fire({
            type: 'success',
            title: `The admin ${admin.strName} has been deleted!`,
          });
          return this.getDataTableAdmins();
        }).catch(() => {
          return Toast.fire({
            type: 'error',
            title: 'Oops... sorry we have some problems! :(',
          });
        });
      }
    });
  }   

}