import { Component, OnInit, Input } from '@angular/core';
import { ProductsModel, ProductsInterface } from '../../../models/products.model';
import { ProductsService } from './../../../services/products.service';
import { ActivatedRoute } from '@angular/router'
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Component({
  selector: 'app-modify-products',
  templateUrl: './modify-products.component.html',
  styleUrls: ['./modify-products.component.css']
})
export class ModifyProductsComponent implements OnInit {

  @Input() products: ProductsModel;

  product: ProductsInterface = {
    nmbCode: null,
    strName: null,
    nmbPrice: null,
    nmbQuantity: null,
    strBrand: null,
    strDescription: null
  };

  constructor(private productsService: ProductsService, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
  }

  modifyProducts( form: NgForm ) {
    if (form.invalid) {
      Swal.fire({
        title:'Oops...',
        text: 'Fill all the fields to continue',
        type:'error'
      });
      return null;
    } 
    Swal.fire({
      title: 'Are you sure?',
      text: 'You can modify in the future!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, add it!'
    }).then((result) => {
      if (result.value) {
        Toast.fire({
          type: 'success',
          title: `The product ${this.product.strName} has been updated!`,
        });
      console.log(this.product);  
      }
    });
  }

}