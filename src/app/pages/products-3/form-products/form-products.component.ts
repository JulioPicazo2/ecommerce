import { Component, OnInit } from '@angular/core';
import { ProductsModel, ProductsInterface } from '../../../models/products.model';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { ProductsService } from 'src/app/services/products.service';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Component({
  selector: 'app-form-products',
  templateUrl: './form-products.component.html',
  styleUrls: ['./form-products.component.css']
})
export class FormProductsComponent implements OnInit {

  product: ProductsModel = new ProductsModel();

  constructor(private productsService: ProductsService){
  }

  ngOnInit() {
  }

  addProducts( form: NgForm ) {
    if (form.invalid) {
      return Swal.fire({
        title:'Oops...',
        text: 'Fill all the fields to continue',
        type:'error'
      });
    } else {
      Swal.fire({
        title: 'Are you sure?',
        text: 'You can modify in the future!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, add it!'
      }).then((result) => {
        if (result.value) {
          this.productsService.addProduct(this.product).then((data) => {
            Toast.fire({
              type: 'success',
              title: `The product ${this.product.strName} has been added!`,
            });  
            return form.reset();
          }).catch(() => {
            return Toast.fire({
              type: 'error',
              title: 'Oops... sorry we have some problems! :(',
            });
          });
        }
      });
    }
  }

}