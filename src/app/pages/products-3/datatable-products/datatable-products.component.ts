import { Component, OnInit, Input, Output } from '@angular/core';
import { ProductsService } from './../../../services/products.service';
import { ProductsModel } from 'src/app/models/products.model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});
  
@Component({
  selector: 'app-datatable-products',
  templateUrl: './datatable-products.component.html',
  styleUrls: ['./datatable-products.component.css']
})
export class DatatableProductsComponent implements OnInit {

  actualPage: number = 1;
  products: ProductsModel[] = [];

  constructor( private productsService: ProductsService, private router: Router ) { }

  ngOnInit() {
    this.getDataTableProducts();
  }
  
  getDataTableProducts() {
   this.productsService.dataTableProducts().then((datos: ProductsModel[]) => {
      this.products = datos;
    }).catch(() => {
      return Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Connection lost'
      });
    });
  }

  deleteProduct( product: ProductsModel ) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.productsService.deleteProduct(product.nmbCode).then(data => {
          Toast.fire({
            type: 'success',
            title: `The product ${product.strName} has been deleted!`,
          });
          return this.getDataTableProducts();
        }).catch(() => {
          return Toast.fire({
            type: 'error',
            title: 'Oops... sorry we have some problems! :(',
          });
        });
      }
    });
  }

  modifyProduct( product: ProductsModel ) {
    this.router.navigate(['/modify-products', product.nmbCode]);    
  }

}
