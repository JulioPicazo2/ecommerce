import { Component, OnInit } from '@angular/core';
import { ProductsModel } from './../../models/products.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  data: Sell [] = [];

  constructor() { }

  ngOnInit() {
  }

}

export interface Sell {
  date: Date,
  user: string,
  quantity: number,
  product: ProductsModel,
}