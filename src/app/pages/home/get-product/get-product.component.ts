import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductsModel } from 'src/app/models/products.model';
import { SellsModel } from 'src/app/models/sells.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-get-product',
  templateUrl: './get-product.component.html',
  styleUrls: ['./get-product.component.css']
})
export class GetProductComponent implements OnInit {

  products: ProductsModel;
  form: FormGroup;
  @Input() data: Sell;
  constructor() {
    this.form = new FormGroup({
      'dteDate': new FormControl('', [Validators.required]),
    })
  }

  ngOnInit() {
    
  }

  pushProduct() {
    if (this.form.invalid) {
      Swal.fire({
        type: 'error',
        title: 'Invalid form',
        text: 'Fill all the fields to continue'
      });
    }
  }
}


export interface Sell {
  date: Date,
  user: string,
  quantity: number,
  product: ProductsModel,
}