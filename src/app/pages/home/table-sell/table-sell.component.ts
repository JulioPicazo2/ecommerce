import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-sell',
  templateUrl: './table-sell.component.html',
  styleUrls: ['./table-sell.component.css']
})
export class TableSellComponent implements OnInit {

  @Input() data;

  constructor() { }

  ngOnInit() {
  }

}
