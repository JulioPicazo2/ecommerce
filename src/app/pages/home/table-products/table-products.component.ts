import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ProductsService } from './../../../services/products.service';
import { ProductsModel } from './../../../models/products.model';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Component({
  selector: 'app-table-products',
  templateUrl: './table-products.component.html',
  styleUrls: ['./table-products.component.css']
})
export class TableProductsComponent implements OnInit {

  @Input() data;
  @Output() getProducts = new EventEmitter;

  products: ProductsModel [] = [];
  actualPage = 1;

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.getDataTableProducts();
  }

  getDataTableProducts() {
    this.productsService.dataTableProducts().then((datos: ProductsModel[]) => {
       this.products = datos; 
       console.log(this.products);
     }).catch(() => {
       Swal.fire({
         type: 'error',
         title: 'Oops...',
         text: 'Connection lost'
       });
     });
   }

   getProduct() {

   }
   

}
