import { Component, OnInit, Input } from '@angular/core';
import { ProductsModel } from '../../../models/products.model';
import { ProductsService } from './../../../services/products.service';
import { ActivatedRoute } from '@angular/router'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Component({
  selector: 'app-modify-products',
  templateUrl: './modify-products.component.html',
  styleUrls: ['./modify-products.component.css']
})
export class ModifyProductsComponent implements OnInit {

  products: ProductsModel = new ProductsModel();
  form: FormGroup;

  constructor(private productsService: ProductsService, private activatedRoute: ActivatedRoute) {    
    this.form = new FormGroup({
      'nmbCode': new FormControl(this.products.nmbCode, [Validators.required, Validators.maxLength(10)]),
      'strName': new FormControl(this.products.strName, [Validators.required]),
      'nmbPrice': new FormControl(this.products.nmbPrice, [Validators.required, 
                                      Validators.min(.1)]),
      'nmbQuantity': new FormControl(this.products.nmbQuantity, [Validators.required,
                                          Validators.min(0)]),
      'strBrand': new FormControl(this.products.strBrand, [Validators.required]),
      'strDescription': new FormControl(this.products.strDescription, [Validators.required])
    });
  }

  ngOnInit() {
    const nmbCode = this.activatedRoute.snapshot.paramMap.get('nmbCode');
    this.productsService.getProduct(nmbCode).then((product: ProductsModel) => {
      this.products = product;
      this.products.nmbCode = product.nmbCode;
    }).catch(() => {
      
    });
  }
  
  modifyProduct() {
  if (this.form.invalid) {
    return Swal.fire({
      title:'Oops...',
      text: 'Fill all the fields to continue',
      type:'error'
    });
  } else {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You can modify in the future!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, add it!'
    }).then((result) => {
      if (result.value) {
        this.products = this.form.value;
        this.productsService.modifyProduct(this.products).then((data) => {
          Toast.fire({ 
            type: 'success',
            title: `The product ${this.products.strName} has been updated.`
          });
        }).catch(() => {
          return Toast.fire({
            type: 'error',
            title: 'Oops... sorry we have some problems! :(',
          });
        });
      }
    });
    }
  }

}