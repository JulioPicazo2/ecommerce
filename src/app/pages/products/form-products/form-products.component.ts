import { Component, OnInit } from '@angular/core';
import { ProductsModel } from '../../../models/products.model';
import { ProductsService } from 'src/app/services/products.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000
});

@Component({
  selector: 'app-form-products',
  templateUrl: './form-products.component.html',
  styleUrls: ['./form-products.component.css']
})
export class FormProductsComponent implements OnInit {
  products: ProductsModel = new ProductsModel();
  form: FormGroup;
  disabled: Boolean = false;

  constructor(private productsService: ProductsService, private activatedRoute: ActivatedRoute, private router: Router) {    
    this.form = new FormGroup({
      'nmbCode': new FormControl(this.products.nmbCode, [Validators.required, Validators.maxLength(10)]),
      'strName': new FormControl(this.products.strName, [Validators.required]),
      'nmbPrice': new FormControl(this.products.nmbPrice, [Validators.required, 
                                      Validators.min(.1)]),
      'nmbQuantity': new FormControl(this.products.nmbQuantity, [Validators.required,
                                          Validators.min(0)]),
      'strBrand': new FormControl(this.products.strBrand, [Validators.required]),
      'strDescription': new FormControl(this.products.strDescription, [Validators.required])
    });
  }

  ngOnInit() {
    const nmbCode = this.activatedRoute.snapshot.params.id;
    if (nmbCode !== 'new') {
      this.productsService.getProduct(nmbCode).then((product: ProductsModel) => {
        this.products = product['producto'];
        this.disabled = true;
        this.form = new FormGroup({
          'nmbCode': new FormControl(this.products.nmbCode, [Validators.required, Validators.maxLength(10)]),
          'strName': new FormControl(this.products.strName, [Validators.required]),
          'nmbPrice': new FormControl(this.products.nmbPrice, [Validators.required, 
                                          Validators.min(.1)]),
          'nmbQuantity': new FormControl(this.products.nmbQuantity, [Validators.required,
                                              Validators.min(0)]),
          'strBrand': new FormControl(this.products.strBrand, [Validators.required]),
          'strDescription': new FormControl(this.products.strDescription, [Validators.required])
        });
        
      }).catch(() => {
        return Toast.fire({
          type: 'error',
          title: 'Oops... sorry we have some problems! :(',
        });
      });
    }
  }

  modifyProducts() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You can modify in the future!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, add it!'
    }).then((result) => {
      this.products = this.form.value;
      this.productsService.modifyProduct(this.products).then((data) => {
        Toast.fire({
          type: 'success',
          title: `The product ${this.products.strName} has been modified`
        });
      });
    }).catch(() => {
      Toast.fire({
        type: 'error',
        title: 'No jala'
      });
    })
  }

  addProduct() {
    if (this.form.invalid) {
      return Swal.fire({
        title:'Oops...',
        text: 'Fill all the fields to continue',
        type:'error'
      });
    } if(this.products.nmbCode != null) {
      this.modifyProducts();
    } else {
      Swal.fire({
        title: 'Are you sure?',
        text: 'You can modify in the future!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, add it!'
      }).then((result) => {
        if (result.value) {
          this.products = this.form.value;
          this.productsService.addProduct(this.products).then((data) => {
            Swal.fire({ 
              type: 'success',
              title: `Success`,
              text: `The product ${this.products.strName} has been added. ` +
              'Dou  yow want to add another?',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, want it!',
              cancelButtonText: 'No, thanks'      
            }).then((result) => {
              if(result.value) {
                this.form.reset();
              } else {
                this.router.navigate(['/products'])
              }
            });
          }).catch(() => {
            return Toast.fire({
              type: 'error',
              title: 'Oops... sorry we have some problems! :(',
            });
          });
        }
      });
    }
  }

}