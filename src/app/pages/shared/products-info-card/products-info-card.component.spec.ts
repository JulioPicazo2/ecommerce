import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsInfoCardComponent } from './products-info-card.component';

describe('ProductsInfoCardComponent', () => {
  let component: ProductsInfoCardComponent;
  let fixture: ComponentFixture<ProductsInfoCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsInfoCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsInfoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
