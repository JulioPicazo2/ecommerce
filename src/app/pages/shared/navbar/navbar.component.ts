import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

  logout() {
    Swal.fire({
      title: 'Are you sure?',
      text: "Are  you really want to logout?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, i want it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'See you later!',
          'Come back soon.',
          'success'
        )
        this.router.navigate(['/login']);
      }
    });
  }

}
