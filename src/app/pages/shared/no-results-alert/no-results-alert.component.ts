import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-results-alert',
  templateUrl: './no-results-alert.component.html',
  styleUrls: ['./no-results-alert.component.css']
})
export class NoResultsAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
