import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule } from '@angular/common/http';

//Login
import { LoginComponent } from './pages/login/login.component';

//Home
import { HomeComponent } from './pages/home/home.component';
//Ticket
import { TicketComponent } from './pages/ticket/ticket.component';
//NotFound
import { NotFoundComponent } from './pages/not-found/not-found.component';
//Productos
/* Inicio productos */import { ProductsComponent } from './pages/products/products.component';
import { DatatableProductsComponent } from './pages/products/datatable-products/datatable-products.component';
    import { FormProductsComponent } from './pages/products/form-products/form-products.component';
//Administradores
/* Inicio productos */import { AdminsComponent } from './pages/admins/admins.component';
import { DatatableAdminsComponent } from './pages/admins/datatable-admins/datatable-admins.component';
import { FormAdminsComponent } from './pages/admins/form-admins/form-admins.component';

//Shared
import { NavbarComponent } from './pages/shared/navbar/navbar.component';
import { AlertFormsComponent } from './pages/shared/alert-forms/alert-forms.component';
import { ProductsInfoCardComponent } from './pages/shared/products-info-card/products-info-card.component';
import { AdminsInfoCardComponent } from './pages/shared/admins-info-card/admins-info-card.component';
import { NoResultsAlertComponent } from './pages/shared/no-results-alert/no-results-alert.component';

//Pipes
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

//Fechas de PIPES en español
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { CommingSoonComponent } from './pages/comming-soon/comming-soon.component';
import { ModifyProductsComponent } from './pages/products/modify-products/modify-products.component';
import { VerticalMenuComponent } from './pages/shared/vertical-menu/vertical-menu.component';
import { ModifyAdminsComponent } from './pages/admins/modify-admins/modify-admins.component';
import { SellsComponent } from './pages/sells/sells.component';
import { GetProductComponent } from './pages/home/get-product/get-product.component';
import { TableProductsComponent } from './pages/home/table-products/table-products.component';
import { TableSellComponent } from './pages/home/table-sell/table-sell.component';
import { DatatableSellsComponent } from './pages/sells/datatable-sells/datatable-sells.component';
registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    TicketComponent,
    NotFoundComponent,
    ProductsComponent,
    DatatableProductsComponent,
    FormProductsComponent,
    AlertFormsComponent,
    NoResultsAlertComponent,
    ProductsInfoCardComponent,
    AdminsComponent,
    DatatableAdminsComponent,
    AdminsInfoCardComponent,
    FormAdminsComponent,
    CommingSoonComponent,
    ModifyProductsComponent,
    VerticalMenuComponent,
    ModifyAdminsComponent,
    SellsComponent,
    GetProductComponent,
    TableProductsComponent,
    TableSellComponent,
    DatatableSellsComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    Ng2SearchPipeModule,
    NgxPaginationModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es',}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
