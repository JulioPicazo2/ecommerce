
export class SellsModel {
    public id_venta: string;
    public nmbCode:string;
    public nmbID: string;
    public nmbCantidad: string;
    public fltTotal: string;
    public dteDate:Date;
}

export interface SellsInterface {
    id_venta: string,
    nmbCode:string,
    nmbID:string,
    cantidad: string,
    total: string,
    fecha: Date
}
