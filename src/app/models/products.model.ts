export class ProductsModel {
    public nmbCode: string;
    public strName: string;
    public nmbPrice: number;
    public nmbQuantity: number;
    public strBrand: string;
    public strDescription: string;
}

export interface ProductsInterface {
    nmbCode: string,
    strName: string,
    nmbPrice: number,
    nmbQuantity: number,
    strBrand: string,
    strDescription: string
}