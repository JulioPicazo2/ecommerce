export class AdminsModel {
    public nmbID: string;
    public strLastName: string;
    public strSecondLastName: string;
    public strName: string;
    public strUsername: string;
    public strCellphone: string;
    public strEmail: string;
    public strPassword: string;
    public strAddress: string;
    public strAddress2: string;
    public strCountry: string;
    public strState: string;
    public strCity: string;
    public strZip: string;
}

export interface AdminsInterface {
    nmbID: string,
    strLastName:string,
    strSecondLastName:string,
    strName: string,
    strUsername: string,
    strCellphone:string,
    strEmail:string,
    strPassword: string,
    strPasswordConfirm?: string,
    strAddress:string,
    strAddress2:string,
    strCountry:string,
    strState:string,
    strCity:string,
    strZip: string
}